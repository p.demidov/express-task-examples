package ru.iteco.training.examples.springliquibase;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcOperations;
import ru.iteco.training.examples.springliquibase.config.DataSourceConfig;
import ru.iteco.training.examples.springliquibase.config.LiquibaseConfig;

public class Application {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(
                DataSourceConfig.class,
                LiquibaseConfig.class);

        JdbcOperations jdbc = context.getBean(JdbcOperations.class);
        jdbc.queryForList("SELECT test_column FROM test_table").forEach(System.out::println);
    }
}

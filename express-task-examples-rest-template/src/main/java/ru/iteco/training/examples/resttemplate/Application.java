package ru.iteco.training.examples.resttemplate;

import java.util.Collections;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestOperations;
import ru.iteco.training.examples.resttemplate.config.AppConfig;
import ru.iteco.training.examples.resttemplate.model.Joke;

public class Application {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        RestOperations restOperations = context.getBean(RestOperations.class);

        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth("user", "pass");
        HttpEntity<Void> request = new HttpEntity<Void>(headers);
        ResponseEntity<Joke> response = restOperations.exchange(
                "https://api.chucknorris.io/jokes/random",
                HttpMethod.GET,
                request,
                Joke.class,
                Collections.singletonMap("category", "dev"));

        System.out.println(response.getBody().getValue());
    }
}

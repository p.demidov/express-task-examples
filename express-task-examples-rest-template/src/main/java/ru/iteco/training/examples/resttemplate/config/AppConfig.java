package ru.iteco.training.examples.resttemplate.config;

import java.util.Collections;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AppConfig {

    @Bean
    public RestOperations restOperations() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setMessageConverters(Collections.singletonList(converter));
        return restTemplate;
    }
}

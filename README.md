### 1. Мульти модульный Maven проект.
https://books.sonatype.com/mvnex-book/reference/multimodule.html

### 2. Модуль express-task-examples-repository
Пример реализации репозитория с помощью JPA и Hibernate.
Запуск - main класс.

### 3. Модуль express-task-examples-rest-template
Пример использования RestTemplate.
Запуск - main класс.

### 4. Модуль express-task-examples-spring-liquibase
Пример использования Liquibase. 
Скрипты Liquibase запускаются при инициализации Spring контекста.
Запуск - main класс.

### 5. Модуль express-task-examples-spring-security
Пример конфигурации базовой HTTP аутентификации с помощью Spring Security.
Ресурс http://localhost:8080/public.html доступен всем пользователям.
Ресурс http://localhost:8080/private.html доступен только авторизованным пользователям.
Запуск - mvn jetty:run.

### 6. Модуль express-task-examples-swagger-ui
Пример конфигурации Swagger UI.
Интерфейс Swagger UI доступен по адресу http://localhost:8080/swagger-ui.html.
Запуск - mvn jetty:run.


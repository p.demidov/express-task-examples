package ru.iteco.examples.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.examples.repository.entity.TestEntity;

@Repository
public class TestRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public TestEntity save(TestEntity entity) {
        return entityManager.merge(entity);
    }

    @Transactional
    public List<TestEntity> saveAll(List<TestEntity>  entities) {
        List<TestEntity> result = new ArrayList<TestEntity>();

        for (TestEntity entity : entities) {
            result.add(save(entity));
        }

        return result;
    }

    public Optional<TestEntity> findById(Long id) {
        return Optional.ofNullable(entityManager.find(TestEntity.class, id));
    }

    public List<TestEntity> findAll() {
        return entityManager.createQuery("from TestEntity").getResultList();
    }

    public long count() {
        return entityManager.createQuery("select count(*) from TestEntity", Long.class).getSingleResult();
    }

    @Transactional
    public void delete(TestEntity entity) {
        entityManager.remove(entityManager.getReference(TestEntity.class, entity.getId()));
    }

    @Transactional
    public void deleteAll() {
        entityManager.createQuery("delete from TestEntity").executeUpdate();
    }
}

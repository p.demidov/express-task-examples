package ru.iteco.examples.repository.main;

import java.util.Arrays;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.iteco.examples.repository.TestRepository;
import ru.iteco.examples.repository.config.DataSourceConfig;
import ru.iteco.examples.repository.config.JpaConfig;
import ru.iteco.examples.repository.entity.TestEntity;

public class Application {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(
                DataSourceConfig.class,
                JpaConfig.class);

        TestRepository repository = context.getBean(TestRepository.class);

        TestEntity entity1 = new TestEntity();
        entity1.setTestColumn("1");
        TestEntity entity2 = new TestEntity();
        entity2.setTestColumn("2");
        TestEntity entity3 = new TestEntity();
        entity3.setTestColumn("3");
        TestEntity entity4 = new TestEntity();
        entity4.setTestColumn("4");

        entity1 = repository.save(entity1);
        System.out.println("Entity saved with id = " + entity1.getId());

        List<TestEntity> saved = repository.saveAll(Arrays.asList(entity2, entity3, entity4));
        for (TestEntity entity : saved) {
            System.out.println("Entity saved with id = " + entity.getId());
        }

        repository.findById(3L).ifPresentOrElse(e -> {
            System.out.println("By id = 3 found entity with value = " + e.getTestColumn());
        }, () -> {
            System.out.println("Entity not found by id = 3");
        });

        repository.findById(5L).ifPresentOrElse(e -> {
            System.out.println("By id = 5 found entity with value = " + e.getTestColumn());
        }, () -> {
            System.out.println("Entity not found by id = 5");
        });

        System.out.println("Found " + repository.findAll().size() + " entities");

        repository.delete(entity1);

        System.out.println("Count " + repository.count() + " entities");

        repository.deleteAll();

        System.out.println("Count " + repository.count() + " entities");
    }
}

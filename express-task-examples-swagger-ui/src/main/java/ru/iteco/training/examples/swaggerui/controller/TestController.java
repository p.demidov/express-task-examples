package ru.iteco.training.examples.swaggerui.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("/test/{name}")
    public String testGET(@PathVariable String name) {
        return String.format("Hello %s from GET method", name);
    }

    @PostMapping("/test")
    public String testPOST(@RequestBody String name) {
        return String.format("Hello %s from POST method", name);
    }
}
